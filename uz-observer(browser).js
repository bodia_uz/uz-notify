// Restore search inputs
var inputs = JSON.parse(localStorage.getItem('searchInputs') || '[]');
inputs.forEach(input => document.forms.train_search_form.elements[input.name].value = input.value);

// Save search inputs
var inputs = [].map.call(document.forms.train_search_form.elements, ({name, value}) => ({name, value}));
localStorage.setItem('searchInputs', JSON.stringify(inputs));


// Polling
(function(global, searchForm, commonService, trainsService) {
  'use strict';

  const APP_TITTLE = 'РЈРєСЂР·Р°Р»С–Р·РЅРёС†СЏ';

  const NOTIFICATION_GRUNTED = 'granted';
  const NOTIFICATION_DENIED = 'denied';
  const NOTIFICATION_DENIED_MESSAGE = 'Notification permission denied';
  const NOTIFICATION_UNSUPPORTED_MESSAGE = 'Desktop notifications not available in your browser. Try Chromium.';

  const DEFAULT_OBSERVER_MINUTE_INTERVAL = 3;

  // destroy previous observer
  if (typeof global.destroyUZObserver === 'function') {
    global.destroyUZObserver();
  }

  // init observer and export observer destroy function
  global.destroyUZObserver = observe(promptObserverInterval(DEFAULT_OBSERVER_MINUTE_INTERVAL));

  function promptObserverInterval(defaultValue) {
    let interval = parseInt(prompt('Observer interval (minutes)?', String(defaultValue)), 10);

    return !interval || isNaN(interval) ? defaultValue : interval;
  }

  function observe(interval) {
    _log('starting search observer..');
    return _observe({
      interval: interval,

      onSearchSuccess(trainsData) {
        _log('search result:\n', _trainsDataToString(trainsData));
        trainsService.show(trainsData);
      },

      onSearchError(error) {
        _log('search error:', error);
        trainsService.error(error);
      },

      onResultChanged(result) {
        _notify(result);
      }
    });
  }

  //----------------------------------

  function _observe(config) {
    let intervalId;
    let lustResult;

    let _doSearch = () => {
      _ajax('/purchase/search/', searchForm)
          .then((trainsData) => {
            config.onSearchSuccess(trainsData);

            return _trainsDataToString(trainsData)
          })
          .catch((error) => {
            config.onSearchError(error);

            return _responseToString(error);
          })
          .then((result) => {
            if (!lustResult || result !== lustResult) {
              lustResult = result;

              config.onResultChanged(result);
            }
          });
    };

    _doSearch();

    intervalId = setInterval(_doSearch, (config.interval || DEFAULT_OBSERVER_MINUTE_INTERVAL) * 60 * 1000);

    return function destroyObserver() {
      clearInterval(intervalId);
    };
  }

  function _trainsDataToString(trainsData) {
    return Array.isArray(trainsData) ? _trainsArrayToString(trainsData) : _responseToString(trainsData);
  }

  function _trainsArrayToString(trains) {
    return trains.map((train) => {
      return train.num + ': ' + train.types.map((t) => t.letter + t.places).join(', ');
    }).join('\n');
  }

  function _responseToString(trainsError) {
    if (!trainsError) {
      return 'NO_DATA';
    }

    if (typeof trainsError === 'string') {
      return trainsError;
    }

    return JSON.stringify(trainsError);
  }

  function _ajax(url, formElement) {
    return new Promise((resolve, reject) => {
      commonService.ajax(url, {form: formElement}, (resp) => resp.error ? reject(resp.value) : resolve(resp.value));
    });
  }

  function _log() {
    let args =  Array.prototype.slice.call(arguments);
    let time = '[' + new Date().toTimeString().split(' ')[0] + ']';

    args.unshift(time);

    console.log.apply(console, args);
  }

  function _notify(title, message, favicon) {
    _initNotifications()
      .then(() => {

        let notification = new Notification(title, {
          icon: favicon || '/favicon.ico',
          body: message
        });

        notification.onclick = function() {
          notification.close();
          self.focus();
        };

      })
      .catch((error) => {
        console.warn(error);
      });
  }

  function _initNotifications() {
    return new Promise((resove, reject) => {

      switch (Notification && Notification.permission) {
        case undefined:
          reject(NOTIFICATION_UNSUPPORTED_MESSAGE);

          return;

        case NOTIFICATION_DENIED:
          reject(NOTIFICATION_DENIED_MESSAGE);

          return;

        case NOTIFICATION_GRUNTED:
          resove();
          return;

        default:
          Notification.requestPermission((permission) => {
            if (permission === NOTIFICATION_GRUNTED) {
              resove();
            } else {
              reject(NOTIFICATION_DENIED_MESSAGE);
            }
          });
      }
    });
  }

})(window, document.forms.train_search_form, Common, new TTrains());