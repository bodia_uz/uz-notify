import request from 'request';

// require('request-debug')(request);

function requestPromise(url, method, options) {
  return new Promise((resolve, reject) => {
    request({...options, json: true, url, method}, (error, httpResponse, body) => {
      if (error || !body || body.error) {
        reject(error || (body && body.value) || body);
      } else {
        resolve(body);
      }
    });
  });
}

function post(url, options) {
  return requestPromise(url, 'POST', options);
}

function get(url, options) {
  return requestPromise(url, 'GET', options);
}

function createCookieJar(cookieString, url) {
  const jar = request.jar();

  jar.setCookie(cookieString, url);
  
  return jar;
}

export { get, post, createCookieJar };
