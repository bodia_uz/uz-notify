import * as http from './http';

const BASE_URL = 'http://booking.uz.gov.ua';

class BookingClient {
  constructor({baseUrl = BASE_URL, token, sessionId}) {
    this._baseUrl = baseUrl;
    this._stationsCache = {};

    this.setAuth(token, sessionId);
  }

  setAuth(token, sessionId) {
    const baseUrl = this._baseUrl;
    const jar = http.createCookieJar(`_gv_sessid=${sessionId}; _gv_lang=uk`, baseUrl);
    const headers = {
      'GV-Ajax':1,
      'GV-Referer': baseUrl,
      //'GV-Referer-Src': 'http://www.uz.gov.ua/',
      //'GV-Referer-Src-Jump': 1,
      //'GV-Screen': '2560x1440',
      'GV-Token': token
      //'GV-Unique-Host': 1
    };

    this._config = { headers, jar };
  }

  getStationId(query) {
    return this.getStation(query).then(station => station.id);
  }

  getStationName(query) {
    return this.getStation(query).then(station => station.name);
  }

  getStation(query) {
    return this.getStations(query).then(stations => !stations.length ? Promise.reject(404) : stations[0]);
  }

  getStations(query) {
    const { headers, jar } = this._config;
    const url = `${this._baseUrl}/purchase/station/?term=${encodeURIComponent(query.toLowerCase())}`;

    if (this._stationsCache.hasOwnProperty(url)) {
      return Promise.resolve(this._stationsCache[url]);
    }

    return http.get(url, { headers, jar })
        .then(results => {
          const stations = results.map(({ value, title}) => ({
            id: value,
            name: title
          }));

          this._stationsCache[url] = stations;

          return stations;
        });
  }

  search({stationFromId, stationToId, departureDate, departureTime = '00:00', coachTypes = ['Л', 'К', 'П']}) {
    const { headers, jar } = this._config;
    const url = `${this._baseUrl}/purchase/search/`;
    const form = {
      station_id_from: stationFromId,
      station_id_till: stationToId,
      //station_from: 'Київ',
      //station_till: 'Ужгород',
      date_dep: departureDate,
      time_dep: departureTime,
      time_dep_till: '',
      another_ec: 0,
      search: ''
    };

    return http
        .post(url, { headers, jar, form })
        .then(results => {
          return !Array.isArray(results.value) ?
              results :
              results
                .value
                .map(train => ({
                  ...train,
                  types: train.types.filter(t => coachTypes.indexOf(t.letter) !== -1)
                }))
                .filter(train => train.types.length);

        });
  }
}

export default BookingClient;
