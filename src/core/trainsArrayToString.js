function trainTypeToString(trainType) {
  return trainType.letter + trainType.places;
}

function trainToString(train) {
  return train.num + ': ' + train.types.map(trainTypeToString).join(', ');
}

function trainsArrayToString(trains) {
  return trains.map(trainToString).join('\n');
}

export default trainsArrayToString;