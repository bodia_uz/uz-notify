import Horseman from 'node-horseman';
import { parse as parseCookie } from 'cookie';

const HORSEMAN_CONFIG = {
  loadImages: false,
  injectJquery: false
};

async function getTokenAndSessionId() {
  const { cookie, token } = await new Horseman(HORSEMAN_CONFIG)
      .open('http://booking.uz.gov.ua')
      .evaluate(function () {
        return {
          cookie: document.cookie,
          token: localStorage.getItem('gv-token')
        };
      });

  const sessionId = parseCookie(cookie)['_gv_sessid'];

  return { sessionId, token };
}

export default getTokenAndSessionId;
