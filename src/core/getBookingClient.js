import getTokenAndSessionId from './getTokenAndSessionId';
import BookingClient from './BookingClient';

async function getBookingClient() {
  const { token , sessionId} = await getTokenAndSessionId();

  return new BookingClient({ token, sessionId });
}

export default getBookingClient;