import inquirer from 'inquirer';
import moment from 'moment';

function validate(input) {
  return moment(input, 'HH:mm').isValid() || 'Вкажіть коректний час.\n'
}

/**
 * @param {string} message
 * @param {string} [defaultValue]
 * @returns {Promise}
 */
async function promptDepartureTime(message, defaultValue = '00:00') {
  const { departureTime } = await inquirer.prompt({
    type: 'input',
    name: 'departureTime',
    message: message,
    default: defaultValue,
    validate: validate
  });

  return departureTime;
}

export default promptDepartureTime;
