import promptStation from './promptStation';
import promptDepartureDate from './promptDepartureDate';
import promptDepartureTime from './promptDepartureTime';
import promptCoachTypes from './promptCoachTypes';

/**
 * @param {BookingClient} bookingClient
 * @param {string} stationFrom
 * @param {string} stationTo
 * @param {string} date
 * @param {string} time
 * @returns {Promise}
 */
async function promptSearchQuery(bookingClient, {stationFrom = 'Київ', stationTo = 'Ужгород', date, time} = {}) {
  const stationFromId = await promptStation(bookingClient, 'Звідки:', stationFrom);
  const stationToId = await promptStation(bookingClient, 'Куди:', stationTo);
  const departureDate = await promptDepartureDate('Дата відправлення:', date);
  const departureTime = await promptDepartureTime('Час відправлення:', time);
  const coachTypes = await promptCoachTypes('Тип вагону:', time);

  return {
    stationFromId,
    stationToId,
    departureDate,
    departureTime,
    coachTypes
  };
}

export default promptSearchQuery;
