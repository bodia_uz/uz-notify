import inquirer from 'inquirer';

async function promptStationFromStations(message, stations) {
  const { stationId } = await inquirer.prompt({
    type: 'list',
    name: 'stationId',
    message: message,
    choices: stations.map(station => ({name: station.name, value: station.id}))
  });
  
  return stationId;
}

/**
 * @param {BookingClient} client
 * @param {string} message
 * @param {string} [defaultValue]
 * @returns {Promise}
 */
async function promptStation(client, message, defaultValue) {
  const { stationName } = await inquirer.prompt({
    type: 'input',
    name: 'stationName',
    message: message,
    default: defaultValue,
    validate: (value) => value.length > 0
  });
  
  const stations = await client.getStations(stationName);
  const station = stations[0];

  if (!station) {
    return promptStation(client, message, defaultValue);
  }

  if (station.name !== stationName) {
    return promptStationFromStations(message, stations);
  }

  return station.id;
}

export default promptStation;
