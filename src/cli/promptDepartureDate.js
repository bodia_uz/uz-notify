import inquirer from 'inquirer';
import moment from 'moment';

const dateFormat = 'DD.MM.YYYY';
const today = moment().startOf('day');
const tomorrow = today.clone().add(1, 'day');

function filter(input) {
  let date = moment(input, dateFormat);

  if (date.isBefore(tomorrow) && date.isSame(tomorrow, 'month')) {
    // if date in past
    // and current month
    // shift to next month
    date.add(1, 'months');
  }

  return date.isValid() ? date.format(dateFormat) : input;
}

function validate(input) {
  let date = moment(input, dateFormat);

  if (!date.isValid()) {
    return 'Вкажіть коректну дату.';
  }

  if (date.isBefore(today)) {
    return 'Вкажіть дату після сьогоднішнього дня.';
  }

  return true;
}

/**
 * @param {string} message
 * @param {string} [defaultValue]
 * @returns {Promise}
 */
async function promptDepartureDate(message, defaultValue = tomorrow.format('DD.MM.YYYY')) {
  const { departureDate } = await inquirer.prompt({
    type: 'input',
    name: 'departureDate',
    message: message,
    default: defaultValue,
    filter: filter,
    validate: validate
  });

  return departureDate;
}

export default promptDepartureDate;
