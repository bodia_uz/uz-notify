import chalk from 'chalk';
import moment from 'moment';
import { diffWords } from 'diff';

import { trainsArrayToString } from '../core'

function beep() {
  process.stdout.write('\x07');
}

function time() {
  const  now = moment().format('HH:mm');

  console.log(chalk.dim(`-----${now}-----`));
}

function log(message) {
  console.log(chalk.bold(message));
}

function colorDiff(prevTrains, currentTrains) {
  return diffWords(prevTrains, currentTrains).map(part => {
    if (part.removed) {
      return '';
    }

    return part.added ? chalk.green(part.value) : part.value;
  }).join('');
}


function error(message) {
  console.log(chalk.red.bold('error:'), message);
}

function createReporter() {
  let prevTrains = '';

  function result(data) {
    let message;
    let style;

    if (typeof data === 'string') {

      message = data;
      style = chalk.yellow;

    } else if (Array.isArray(data)) {


      let trains = trainsArrayToString(data);

      message = prevTrains ? colorDiff(prevTrains, trains) : trains;
      prevTrains = trains;

      style = chalk.bold;

    } else {

      message = JSON.stringify(data);
      style = chalk.bold;
    }

    console.log(style(message));
  }

  return {
    log,
    time,
    result,
    error,
    beep
  }
}

export default createReporter;