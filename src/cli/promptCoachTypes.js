import inquirer from 'inquirer';

/**
 * @param {string} message
 * @param {string[]} defaultValues
 * @returns {Promise}
 */
async function promptCoachTypes(message, defaultValues = ['Л', 'П', 'К']) {
  const { types } = await inquirer.prompt({
    type: 'checkbox',
    name: 'types',
    message: message,
    default: defaultValues,
    choices: [
      {value: 'Л', name: 'Люкс'},
      {value: 'К', name: 'Купе'},
      {value: 'П', name: 'Плацкарт'}
    ],
    validate: function (answer) {
      if (answer.length < 1) {
        return 'Оберіть хочаб один тип вагону.';
      }
      return true;
    }
  });

  return types;
}

export default promptCoachTypes;
