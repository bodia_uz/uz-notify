#!/usr/bin/env node

import {Observable} from 'rxjs/Rx';
import notifier from 'node-notifier';

import { getTokenAndSessionId, BookingClient, trainsArrayToString } from '../core';
import promptSearchQuery from './promptSearchQuery';
import createReporter from './createReporter';

async function start() {
  const reporter = createReporter();

  reporter.log('Зачекайте...');

  let { token, sessionId } = await getTokenAndSessionId();
  let client = new BookingClient({ token, sessionId });
  let searchQuery = await promptSearchQuery(client);

  let interval = 60 * 1000;

  // use fake core
  // client.setAuth(token, 'blabla');

  const searchWithInterval$ = Observable
      .interval(interval)
      .startWith('run right away')
      .flatMap(x => client.search(searchQuery).catch(error => ({ rejected: true, error })));

  searchWithInterval$
      .do(() => {
          reporter.time();
      })
      .distinctUntilChanged((resultA, resultB) => JSON.stringify(resultA) === JSON.stringify(resultB))
      .subscribe(
          result => {
            reporter.beep();

            if (!result.rejected) {
              reporter.result(result);

              if (Array.isArray(result)) {
                  //https://github.com/mikaelbr/node-notifier
                  notifier
                    .notify({
                      title: 'Квитки',
                      message: trainsArrayToString(result),
                      sound: true,
                      wait: true
                    });
              }

              return;
            }

            reporter.error(result.error);

            getTokenAndSessionId().then(({token, sessionId}) => {
              client.setAuth(token, sessionId);
            });
          },
          error => {
            console.log('Fatal error', error);
          }
      );
}

start();
